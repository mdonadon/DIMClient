# DIMClient
Simple C++ DIM browser and/or client.

## Interactive mode
Basically WebDid but without mouse interaction and no refresh at every single server update. 
To use interactive mode run `./bin/DIMClient --i` and follow the output.

When writing a message `REPLACE '\n' WITH '.'`, if you don't want to rewrite it again enter "r".

In interactive mode, you can activate also monitor mode. This will listen to all _ANS and _ERR services of the selected FRED server (remote logging). It won't work on ALF because of RPCs.

## Command line parameters
 - -interactive or --i for interactive mode
 - -help or -h to print the help menu
 - -ALF or --A to talk to ALF
 - -FRED or --F to talk to FRED
 - -topic or --t \<topic name\> --m [message] to send a DIM command, `DO NOT USE SUFFIXES` ("_REQ", "/RpcIn", etc) and `REPLACE '\n' WITH '.'`
 - -dns or --d to set the DIM DNS, this will overwrite the one specified in the environmental variable DIM_DNS_NODE

## Installation
```
git clone https://gitlab.cern.ch/mdonadon/DIMClient.git

cd DIMClient

source scl_source enable devtoolset-7

cmake3 .

make all
```


## Examples
```
./bin/DIMClient --F --t FRED_DCS_LAB/TPC/FEC3/SCAID/READ
./bin/DIMClient  --A --t ALF_ALDCS075/SERIAL_0/LINK_2/SCA_SEQUENCE  --m 140204d1,00000001
./bin/DIMClient --F --t FRED_DCS_LAB/TPC/FEC3/SCAID/READ --d exampledimdns.cern.ch
./bin/DIMClient --i
./bin/DIMClient --i --d exampledimdns.cern.ch
```