#include <iostream>
#include <string>
#include <vector>
#include "DIMClient/global.h"
#include "DIMClient/dimclient.h"

int main(int argc, char** argv)
{
    Dimclient dimclient(argc, argv);

    dimclient.run();

    return 0;
}
